package com.project.food;

import com.project.Adoption.FirstPage;
import com.project.controller.LoginPage;
import com.project.shop.accesories;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class DogFoodShoppingCartApp extends Application {
    private Image[] images;
    private ImageView currentImageView;
    private ImageView nextImageView;
    private int currentImageIndex = 0;
    private Pane root;

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("adoption");
        Scene scene = CreateScene(primaryStage);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private Cart cart = new Cart();
    private ObservableList<DogFood> products = FXCollections.observableArrayList(
            new DogFood("Himalaya Healthy Pet Food - Adult\n - Meat & Rice", 774,
                    "High-quality dog food for all breeds.",
                    "file:dogodiary\\src\\main\\resources\\images\\food1.jpg"),
            new DogFood("WIGGLES EveryDawg Mother & Puppy \nStarter Dry Dog Food", 899,
                    "Organic ingredients for a healthy diet.",
                    "file:dogodiary\\src\\main\\resources\\images\\food2.jpg"),
            new DogFood("Royal Canin Maxi Adult Dog Dry\n Food", 999, "Specially formulated for growing puppies.",
                    "file:dogodiary\\src\\main\\resources\\images\\food3.jpg"),
            new DogFood("All4Pets Simba With Chicken Dog\n Food ", 599, "High-quality dog food for all breeds.",
                    "file:D:dogodiary\\src\\main\\resources\\images\\food4.jpg"),
            new DogFood("All4Pets Simba With Chicken Dog\n Food ", 599, "High-quality dog food for all breeds.",
                    "file:D:dogodiary\\src\\main\\resources\\images\\food4.jpg"),
            new DogFood("Himalaya Healthy Pet Food - Adult\n - Meat & Rice", 774,
                    "High-quality dog food for all breeds.",
                    "file:dogodiary\\src\\main\\resources\\images\\food1.jpg"),
            new DogFood("WIGGLES EveryDawg Mother & Puppy \nStarter Dry Dog Food", 899,
                    "Organic ingredients for a healthy diet.",
                    "file:dogodiary\\src\\main\\resources\\images\\food2.jpg"),
            new DogFood("Royal Canin Maxi Adult Dog Dry\n Food", 999, "Specially formulated for growing puppies.",
                    "file:dogodiary\\src\\main\\resources\\images\\food3.jpg"),
            new DogFood("All4Pets Simba With Chicken Dog\n Food ", 599, "High-quality dog food for all breeds.",
                    "file:D:dogodiary\\src\\main\\resources\\images\\food4.jpg"));

           
    public class BuyCart extends Application {

        public Scene CreateScene(Stage primaryStage) {

            // Cart ListView
            ListView<DogFood> cartListView = new ListView<>(cart.getItems());

            // Total Price Label
            Label totalPriceLabel = new Label("Total: $0.00");

            // Buy Cart Button
            Button buyCartButton = new Button("Buy Cart");
            buyCartButton.setOnAction(e -> {
                if (!cart.getItems().isEmpty()) {
                    // Perform purchase actions
                    System.out.println("Purchased: " + cart.getItems());
                    cart.clear();
                    cartListView.setItems(cart.getItems());
                    totalPriceLabel.setText("Total: $0.00");
                }
            });
            VBox cartBox = new VBox(10, cartListView, totalPriceLabel, buyCartButton);
            Scene scene = new Scene(cartBox, 1980, 1080);
            return scene;
        }

        @Override
        public void start(Stage primaryStage) throws Exception {
            primaryStage.setTitle("adoption");
            Scene scene = CreateScene(primaryStage);
            primaryStage.setScene(scene);
            primaryStage.show();

        }

    }

    public Scene CreateScene(Stage primaryStage) {
        // Load your sequence of images
        loadImages();

        // Create ImageViews to display the images
        currentImageView = new ImageView(images[0]);
        currentImageView.setFitHeight(300);
        currentImageView.setFitWidth(800);

        nextImageView = new ImageView();
        nextImageView.setFitHeight(300);
        nextImageView.setFitWidth(800);

        // Position the nextImageView outside the scene initially
        nextImageView.setTranslateX(800);

        // Create a layout and add the ImageViews to it
        // HBox main = new HBox(currentImageIndex,n);
        root = new Pane(currentImageView, nextImageView);
        VBox main = new VBox(root);
        main.setPrefHeight(300);
        main.setPrefWidth(800);

        // Set up a Timeline to change the image at regular intervals with sliding
        // effect
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(2), event -> slideImage()));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
        primaryStage.setTitle("Dog Food Shop");

        GridPane grid = new GridPane();
        grid.setLayoutY(1500);
        grid.setHgap(10);
        grid.setVgap(10);
        int col = 0, row = 0;

        for (DogFood product : products) {
            VBox productBox = createProductBox(product);
            grid.add(productBox, col, row);
            col++;
            if (col == 3) { // Two columns layout
                col = 0;
                row++;
            }
        }

        // Cart ListView
        ListView<DogFood> cartListView = new ListView<>(cart.getItems());

        // Total Price Label
        Label totalPriceLabel = new Label("Total: $0.00");

        // Buy Cart Button
        Button buyCartButton = new Button("Buy Cart");
        buyCartButton.setOnAction(e -> {
            if (!cart.getItems().isEmpty()) {
                // Perform purchase actions
                System.out.println("Purchased: " + cart.getItems());
                cart.clear();
                cartListView.setItems(cart.getItems());
                totalPriceLabel.setText("Total: $0.00");
            }
        });
        Image dogoImage = new Image("images/logo.jpg");
        ImageView dogoImageView = new ImageView(dogoImage);
        dogoImageView.setFitHeight(60);
        dogoImageView.setFitWidth(70);
        Button home = new Button("DOGGODIARY.COM");
        home.setTextFill(Color.GREENYELLOW);
        home.setAlignment(Pos.BASELINE_LEFT);
        home.setFont(new Font("Impact", 25));
        home.setMaxHeight(80);
        home.setPrefWidth(350);
        home.setStyle("-fx-background-color:white");
        home.setStyle("-fx-border-color:black");
        // home.setEffect(new DropShadow(20, Color.WHITE));
        home.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
        home.setGraphic(dogoImageView);

        Button homeService = new Button("HOME");
        homeService.setTextFill(Color.GREENYELLOW);
        homeService.setAlignment(Pos.CENTER);
        homeService.setFont(new Font("Serif Regular", 20));
        homeService.setPrefHeight(80);
        homeService.setPrefWidth(190);
        homeService.setStyle("-fx-background-color:white");
        homeService.setStyle("-fx-border-color:black");
        homeService.setEffect(new DropShadow(20, Color.BLACK));
        homeService.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

        Button availPuppy = new Button("AVAILABLE \nPUPPIES");
        availPuppy.setTextFill(Color.GREENYELLOW);
        availPuppy.setAlignment(Pos.CENTER);
        availPuppy.setFont(new Font("Serif Regular", 20));
        availPuppy.setPrefHeight(80);
        availPuppy.setPrefWidth(150);
        availPuppy.setStyle("-fx-background-color:white");
        availPuppy.setStyle("-fx-border-color:black");
        availPuppy.setEffect(new DropShadow(20, Color.BLACK));
        availPuppy.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

        Button adopttion = new Button("ADOPTION");
        adopttion.setTextFill(Color.GREENYELLOW);
        adopttion.setAlignment(Pos.CENTER);
        adopttion.setFont(new Font("Serif Regular", 20));
        adopttion.setPrefHeight(80);
        adopttion.setPrefWidth(150);
        adopttion.setStyle("-fx-background-color:white");
        adopttion.setStyle("-fx-border-color:black");
        adopttion.setEffect(new DropShadow(20, Color.BLACK));
        adopttion.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
        adopttion.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Scene scene = new FirstPage().CreateScene(primaryStage);
                primaryStage.setScene(scene);

            }
        });

        Image shopImage = new Image("images/shop.jpg");
        ImageView cartView = new ImageView(shopImage);
        cartView.setFitHeight(60);
        cartView.setFitWidth(70);
        Button shop = new Button("SHOP");
        shop.setTextFill(Color.GREENYELLOW);
        shop.setAlignment(Pos.CENTER);
        shop.setFont(new Font("Serif Regular", 20));
        shop.setPrefHeight(80);
        shop.setPrefWidth(190);
        shop.setStyle("-fx-border-color:black");
        // shop.setStyle("-fx-background-color:white");
        shop.setEffect(new DropShadow(20, Color.BLACK));
        shop.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
        shop.setGraphic(cartView);
         shop.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                Scene scene = new accesories().CreateScene(primaryStage);
                primaryStage.setScene(scene);
                
                }
            });
        Button login = new Button("LOGIN");
        login.setTextFill(Color.GREENYELLOW);
        login.setAlignment(Pos.CENTER);
        login.setFont(new Font("serif Regular", 20));
        login.setPrefHeight(80);
        login.setPrefWidth(150);
        // login.setStyle("-fx-background-color:yellow");
        login.setStyle("-fx-border-color:black");
        login.setEffect(new DropShadow(20, Color.BLACK));
        login.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

        login.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                Scene scene = new LoginPage().initializeLoginScene(primaryStage);
                primaryStage.setScene(scene);

            }
        });

        Button about = new Button("ABOUT");
        about.setTextFill(Color.GREENYELLOW);
        about.setAlignment(Pos.BASELINE_LEFT);
        about.setFont(new Font("serif regular", 20));
        about.setMaxHeight(80);
        about.setPrefWidth(150);
        about.setStyle("-fx-background-color:white");
        about.setStyle("-fx-border-color:black");
        about.setEffect(new DropShadow(20, Color.BLACK));
        about.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

        HBox hb1 = new HBox(home, homeService, availPuppy, adopttion, shop, login, about);
        hb1.setPrefHeight(80);
        hb1.setPrefWidth(1980);

        Button buyCart = new Button("BUYCART");
        buyCart.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Scene scene = new BuyCart().CreateScene(primaryStage);
                primaryStage.setScene(scene);
                primaryStage.show();
            }
        });

        // Layout
        VBox cartBox = new VBox(10, cartListView, totalPriceLabel, buyCartButton);
        HBox mainLayout = new HBox(grid);
        Label lb2 = new Label("your dog asked for real flavour");
        lb2.setFont(new Font("Impact", 60));
        lb2.setTextFill(Color.RED);
        VBox comBox = new VBox(10, hb1, main, lb2, mainLayout, buyCart);
        for (int i = 0; i < 15; i++) {
            comBox.getChildren().add(new Text(""));

        }
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(comBox);
       // scrollPane.setFitToHeight(false);
        //scrollPane.setFitToWidth(false);
        scrollPane.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));

        Scene scene = new Scene(scrollPane, 1980, 1080);

        // Set up the stage
        return scene;
    }

    private VBox createProductBox(DogFood product) {
        ImageView productImageView = new ImageView(product.getImage());
        productImageView.setFitWidth(150);
        productImageView.setFitHeight(150);

        Label nameLabel = new Label(product.getName());
        nameLabel.setFont(new Font("Book Antiqua", 20));
        Label priceLabel = new Label("₹" + product.getPrice());
        priceLabel.setFont(new Font(20));
        TextArea descriptionArea = new TextArea(product.getDescription());
        descriptionArea.setWrapText(true);
        descriptionArea.setEditable(false);
        descriptionArea.setMaxHeight(30);

        Button addToCartButton = new Button("Add to Cart");
        addToCartButton.setTextFill(Color.WHITE);
        addToCartButton.setOnAction(e -> {
            cart.addProduct(product);
            Label totalPriceLabel = (Label) ((VBox) ((HBox) addToCartButton.getParent().getParent()).getChildren()
                    .get(1)).getChildren().get(1);
            totalPriceLabel.setText("Total: $" + String.format("%.2f", cart.getTotalPrice()));

        });
        addToCartButton.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

        VBox productBox = new VBox(10, productImageView, nameLabel, priceLabel, descriptionArea, addToCartButton);
        productBox.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        // productBox.setEffect(new DropShadow(20, Color.BLACK));
        // productBox.setStyle("-fx-border-color:black");

        return productBox;
    }

    private void slideImage() {
        // Update the next image index
        int nextImageIndex = (currentImageIndex + 1) % images.length;
        nextImageView.setImage(images[nextImageIndex]);

        // Set initial positions for sliding effect
        currentImageView.setTranslateX(0);
        nextImageView.setTranslateX(800);

        // Create the sliding animation
        Timeline slideTimeline = new Timeline(
                new KeyFrame(Duration.ZERO,
                        new KeyValue(currentImageView.translateXProperty(), 0),
                        new KeyValue(nextImageView.translateXProperty(), 800)),
                new KeyFrame(Duration.seconds(1),
                        new KeyValue(currentImageView.translateXProperty(), -800),
                        new KeyValue(nextImageView.translateXProperty(), 0)));

        slideTimeline.setOnFinished(event -> {
            // Update the current image index and swap the ImageViews
            currentImageIndex = nextImageIndex;
            ImageView temp = currentImageView;
            currentImageView = nextImageView;
            nextImageView = temp;
        });

        slideTimeline.play();
    }

    private void loadImages() {
        // Replace with the paths to your images
        String[] imagePaths = {
                "file:dogodiary\\src\\main\\resources\\images\\roll1.jpg",
                "file:dogodiary\\src\\main\\resources\\images\\roll2.jpg",
                "file:dogodiary\\src\\main\\resources\\images\\roll3.jpg",
                // Add more image paths as needed
        };

        images = new Image[imagePaths.length];
        for (int i = 0; i < imagePaths.length; i++) {
            images[i] = new Image(imagePaths[i]);

        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
