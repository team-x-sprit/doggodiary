package com.project.food;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class DogFoodShopApp extends Application {

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Dog Food Shop");

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(10);
        grid.setHgap(10);

        Label titleLabel = new Label("Dog Food Shop");
        titleLabel.setStyle("-fx-font-size: 24px; -fx-font-weight: bold;");
        GridPane.setConstraints(titleLabel, 0, 0, 2, 1);

        VBox productBox1 = createProductBox("Premium Dog Food", "$20.00", "file:path/to/dog_food1.jpg");
        VBox productBox2 = createProductBox("Organic Dog Food", "$25.00", "file:path/to/dog_food2.jpg");

        GridPane.setConstraints(productBox1, 0, 1);
        GridPane.setConstraints(productBox2, 1, 1);

        grid.getChildren().addAll(titleLabel, productBox1, productBox2);

        Scene scene = new Scene(grid, 800, 600);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private VBox createProductBox(String name, String price, String imagePath) {
        VBox vbox = new VBox(10);
        vbox.setPadding(new Insets(10));

        ImageView imageView = new ImageView(new Image("images/a.jpeg"));
        imageView.setFitHeight(150);
        imageView.setFitWidth(150);

        Label nameLabel = new Label(name);
        Label priceLabel = new Label(price);

        Button buyButton = new Button("Buy");
        buyButton.setOnAction(e -> showAlert(name, price));

        vbox.getChildren().addAll(imageView, nameLabel, priceLabel, buyButton);
        return vbox;
    }

    private void showAlert(String productName, String price) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Purchase Confirmation");
        alert.setHeaderText(null);
        alert.setContentText("You have purchased " + productName + " for " + price);
        alert.showAndWait();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
