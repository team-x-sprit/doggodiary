package com.project.food;

import javafx.scene.image.Image;

public class DogFood {
    private String name;
    private double price;
    private String description;
    private Image image;

    public DogFood(String name, double price, String description, String imagePath) {
        this.name = name;
        this.price = price;
        this.description = description;
        this.image = new Image(imagePath);
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public Image getImage() {
        return image;
    }

    @Override
    public String toString() {
        return name + " - ₹" + price;
    }
}
