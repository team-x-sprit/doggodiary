package com.project.food;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Cart {
    private ObservableList<DogFood> items;

    public Cart() {
        items = FXCollections.observableArrayList();
    }

    public void addProduct(DogFood product) {
        items.add(product);
    }

    public ObservableList<DogFood> getItems() {
        return items;
    }

    public double getTotalPrice() {
        return items.stream().mapToDouble(DogFood::getPrice).sum();
    }

    public void clear() {
        items.clear();
    }
}
