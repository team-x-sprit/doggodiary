package com.project.imagetranstion;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;

public class ImageSlidingAnimation extends Application {

    private Image[] images;
    private ImageView currentImageView;
    private ImageView nextImageView;
    private int currentImageIndex = 0;
    private Pane root;

    @Override
    public void start(Stage primaryStage) {
        // Load your sequence of images
        loadImages();

        // Create ImageViews to display the images
        currentImageView = new ImageView(images[0]);
        currentImageView.setFitHeight(300);
        currentImageView.setFitWidth(800);
        
        nextImageView = new ImageView();
        nextImageView.setFitHeight(300);
        nextImageView.setFitWidth(800);

        // Position the nextImageView outside the scene initially
        nextImageView.setTranslateX(800);

        // Create a layout and add the ImageViews to it
       // HBox main = new HBox(currentImageIndex,n);
        root = new Pane(currentImageView, nextImageView);
        HBox main = new HBox(root);
        main.setPrefHeight(300);
        main.setPrefWidth(800);
        

        Scene scene = new Scene(main, 1980, 1080);

        // Set up a Timeline to change the image at regular intervals with sliding effect
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(2), event -> slideImage()));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();

        // Set up the stage
        primaryStage.setTitle("Image Sliding Animation");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void slideImage() {
        // Update the next image index
        int nextImageIndex = (currentImageIndex + 1) % images.length;
        nextImageView.setImage(images[nextImageIndex]);

        // Set initial positions for sliding effect
        currentImageView.setTranslateX(0);
        nextImageView.setTranslateX(800);

        // Create the sliding animation
        Timeline slideTimeline = new Timeline(
                new KeyFrame(Duration.ZERO,
                        new KeyValue(currentImageView.translateXProperty(), 0),
                        new KeyValue(nextImageView.translateXProperty(), 800)),
                new KeyFrame(Duration.seconds(1),
                        new KeyValue(currentImageView.translateXProperty(), -800),
                        new KeyValue(nextImageView.translateXProperty(), 0))
        );

        slideTimeline.setOnFinished(event -> {
            // Update the current image index and swap the ImageViews
            currentImageIndex = nextImageIndex;
            ImageView temp = currentImageView;
            currentImageView = nextImageView;
            nextImageView = temp;
        });

        slideTimeline.play();
    }

    private void loadImages() {
        // Replace with the paths to your images
        String[] imagePaths = {
                "file:dogodiary\\src\\main\\resources\\images\\roll1.jpg",
                "file:dogodiary\\src\\main\\resources\\images\\roll2.jpg",
                "file:dogodiary\\src\\main\\resources\\images\\roll3.jpg",
                // Add more image paths as needed
        };

        images = new Image[imagePaths.length];
        for (int i = 0; i < imagePaths.length; i++) {
            images[i] = new Image(imagePaths[i]);
        
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}

