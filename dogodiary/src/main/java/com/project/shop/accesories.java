package com.project.shop;

import com.project.Adoption.FirstPage;
import com.project.controller.LoginPage;
import com.project.food.DogFoodShoppingCartApp;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class accesories extends Application {
    private Image[] images;
    private ImageView currentImageView;
    private ImageView nextImageView;
    private int currentImageIndex = 0;
    private Pane root;

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("shop");
        Scene scene = CreateScene(primaryStage);
        primaryStage.setScene(scene);
        primaryStage.show();  

    }
    public Scene CreateScene(Stage primaryStage){ 
        primaryStage.setTitle("SHOP");
        loadImages();

        // Create ImageViews to display the images
        currentImageView = new ImageView(images[0]);
        currentImageView.setFitHeight(300);
        currentImageView.setFitWidth(1300);
        
    
        nextImageView = new ImageView();
        nextImageView.setFitHeight(300);
        nextImageView.setFitWidth(1300);

        

        // Create a layout and add the ImageViews to it
        root = new Pane(currentImageView, nextImageView);
        HBox hBox = new HBox(root);
      //  hBox.setAlignment(Pos.CENTER);

        // Set up a Timeline to change the image at regular intervals with sliding
        // effect
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(3), event -> slideImage()));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();

         Image dogoImage = new Image("images/logo.jpg");
        ImageView dogoImageView = new ImageView(dogoImage);
        dogoImageView.setFitHeight(60);
        dogoImageView.setFitWidth(70);
        Button home = new Button("doggodiary");
        home.setTextFill(Color.ORANGERED);
        home.setAlignment(Pos.BASELINE_LEFT);
        home.setFont(new Font("elephant",25));
        home.setMaxHeight(80);
        home.setPrefWidth(350);
        home.setStyle("-fx-background-color:white");
        home.setStyle("-fx-border-color:black");
        home.setEffect(new DropShadow(20, Color.WHITE));
       home.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
       home.setGraphic(dogoImageView);
        
        Button homeService = new Button("HOME");
        homeService.setTextFill(Color.GREENYELLOW);
        homeService.setAlignment(Pos.CENTER);
        homeService.setFont(new Font("Serif Regular",20));
        homeService.setPrefHeight(80);
        homeService.setPrefWidth(190);
        homeService.setStyle("-fx-background-color:white");
        homeService.setStyle("-fx-border-color:black");
        homeService.setEffect(new DropShadow(20, Color.BLACK));
       homeService.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

       Button availPuppy = new Button("AVAILABLE \nPUPPIES");
       availPuppy.setTextFill(Color.GREENYELLOW);
       availPuppy.setAlignment(Pos.CENTER);
       availPuppy.setFont(new Font("Serif Regular",20));
       availPuppy.setPrefHeight(80);
       availPuppy.setPrefWidth(150);
       availPuppy.setStyle("-fx-background-color:white");
       availPuppy.setStyle("-fx-border-color:black");
       availPuppy.setEffect(new DropShadow(20, Color.BLACK));
      availPuppy.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

      Button adopttion = new Button("ADOPTION");
      adopttion.setTextFill(Color.WHITE);
      adopttion.setAlignment(Pos.CENTER);
      adopttion.setFont(new Font("Serif Regular",20));
      adopttion.setPrefHeight(80);
      adopttion.setPrefWidth(150);
      adopttion.setStyle("-fx-background-color:white");
      adopttion.setStyle("-fx-border-color:black");
      adopttion.setEffect(new DropShadow(20, Color.BLACK));
      adopttion.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
      adopttion.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Scene scene = new FirstPage().CreateScene(primaryStage);
                primaryStage.setScene(scene);

            }
        });

        

       
        
        Image shopImage = new Image("images/shop.jpg");
        ImageView cartView = new ImageView(shopImage);
        cartView.setFitHeight(60);
        cartView.setFitWidth(70);
        Button shop = new Button("SHOP");
        shop.setTextFill(Color.GREENYELLOW);
        shop.setAlignment(Pos.CENTER);
        shop.setFont(new Font("Serif Regular",20));  
        shop.setPrefHeight(80);
        shop.setPrefWidth(190); 
        shop.setStyle("-fx-border-color:black");
       // shop.setStyle("-fx-background-color:white");
        shop.setEffect(new DropShadow(20, Color.BLACK));
        shop.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
        shop.setGraphic(cartView);
        
       
        
        Button login = new Button("LOGIN");
        login.setTextFill(Color.GREENYELLOW);
        login.setAlignment(Pos.CENTER);
        login.setFont(new Font("serif Regular",20));
        login.setPrefHeight(80);
        login.setPrefWidth(150);
       // login.setStyle("-fx-background-color:yellow");
        login.setStyle("-fx-border-color:black");
        login.setEffect(new DropShadow(20, Color.BLACK));
       login.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

       login.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            
            Scene scene = new LoginPage().initializeLoginScene(primaryStage);
            primaryStage.setScene(scene);
            
            }
        });
        Button about = new Button("ABOUT");
        about.setTextFill(Color.GREENYELLOW);
        about.setAlignment(Pos.BASELINE_LEFT);
        about.setFont(new Font("serif regular",20));
        about.setMaxHeight(80);
        about.setPrefWidth(150);
        about.setStyle("-fx-background-color:white");
        about.setStyle("-fx-border-color:black");
        about.setEffect(new DropShadow(20, Color.BLACK));
        about.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
        
        
      
      
        HBox hb1 = new HBox(home,homeService,availPuppy,adopttion,shop,login,about);
        hb1.setPrefHeight(80);
        hb1.setPrefWidth(1980);


       
        Image foodImage = new Image("images/vfood.png");
        ImageView foodimageView = new ImageView(foodImage);
        foodimageView.setFitHeight(200);
        foodimageView.setFitWidth(200);

        // Create a Button and set the ImageView as its graphic
        Button foodButton = new Button();
        foodButton.setGraphic(foodimageView);
        foodButton.setEffect(new DropShadow(20, Color.BLACK));

        // Set an action for the button
        foodButton.setOnAction(e -> System.out.println("Button clicked!"));


        Label foodLabel = new Label("FOOD");
        foodLabel.setFont(Font.font("Arial", FontWeight.BOLD, 30));

        VBox foodVBox = new VBox(5, foodButton, foodLabel);
        foodVBox.setAlignment(Pos.CENTER);
        foodButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                Scene scene = new DogFoodShoppingCartApp().CreateScene(primaryStage);
                primaryStage.setScene(scene);
                
                }
            });

        Image toysImage = new Image("images/vtoy.png");
        ImageView toysimageView = new ImageView(toysImage);
        toysimageView.setFitHeight(200);
        toysimageView.setFitWidth(200);


        // Create a Button and set the ImageView as its graphic
        Button toysButton = new Button();
        toysButton.setGraphic(toysimageView);
        toysButton.setEffect(new DropShadow(20, Color.BLACK));

        // Set an action for the button
        toysButton.setOnAction(e -> System.out.println("Button clicked!"));

        Label toysLabel = new Label("TOYS");
        toysLabel.setFont(Font.font("Arial", FontWeight.BOLD, 30));

        VBox toysVBox = new VBox(5, toysButton, toysLabel);
        toysVBox.setAlignment(Pos.CENTER);

        Image matImage = new Image("images/vmat.png");
        ImageView matimageView = new ImageView(matImage);
        matimageView.setFitHeight(200);
        matimageView.setFitWidth(200);


        // Create a Button and set the ImageView as its graphic
        Button matButton = new Button();
        matButton.setGraphic(matimageView);
        matButton.setEffect(new DropShadow(20, Color.BLACK));

        // Set an action for the button
        matButton.setOnAction(e -> System.out.println("Button clicked!"));
        Label matsLabel = new Label("MATS");
        matsLabel.setFont(Font.font("Arial", FontWeight.BOLD, 30));

        VBox matsVBox = new VBox(5, matButton, matsLabel);
        matsVBox.setAlignment(Pos.CENTER);

        Image travelImage = new Image("images/vtravel.png");
        ImageView travelimageView = new ImageView(travelImage);
        travelimageView.setFitHeight(200);
        travelimageView.setFitWidth(200);


        // Create a Button and set the ImageView as its graphic
        Button travelButton = new Button();
        travelButton.setGraphic(travelimageView);
        travelButton.setEffect(new DropShadow(20, Color.BLACK));

        // Set an action for the button
        travelButton.setOnAction(e -> System.out.println("Button clicked!"));
        Label travelLabel = new Label("TRAVEL");
        travelLabel.setFont(Font.font("Arial", FontWeight.BOLD, 30));


        VBox travelVBox = new VBox(5, travelButton, travelLabel);
        travelVBox.setAlignment(Pos.CENTER);

        Image pharmImage = new Image("images/vpharm.png");
        ImageView pharmimageView = new ImageView(pharmImage);
        pharmimageView.setFitHeight(200);
        pharmimageView.setFitWidth(200);


        // Create a Button and set the ImageView as its graphic
        Button pharmButton = new Button();
        pharmButton.setGraphic(pharmimageView);
        pharmButton.setEffect(new DropShadow(20, Color.BLACK));

        // Set an action for the button
        pharmButton.setOnAction(e -> System.out.println("Button clicked!"));
        Label pharmLabel = new Label("PHARMACY");
        pharmLabel.setFont(Font.font("Arial", FontWeight.BOLD, 30));

        VBox pharmVBox = new VBox(5, pharmButton, pharmLabel);
        pharmVBox.setAlignment(Pos.CENTER);

        HBox buttonHBox = new HBox(20, foodVBox, toysVBox, matsVBox, travelVBox, pharmVBox);
       // buttonHBox.setLayoutX(50);
        
        buttonHBox.setAlignment(Pos.CENTER_LEFT);
      //  buttonHBox.setPadding(new Insets(100));

        VBox allVBox = new VBox(20,  hb1,hBox,buttonHBox);
        // allVBox.setStyle("-fx-background-color:beige");

        for (int i = 0; i < 2; i++) {
            allVBox.getChildren().add(new Text(""));

        }
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(allVBox);
       // scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);
        scrollPane.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));


        Scene scene = new Scene(scrollPane, 1980, 1080);
        return scene;
    }

    private void slideImage() {
        // Update the next image index
        int nextImageIndex = (currentImageIndex + 1) % images.length;
        nextImageView.setImage(images[nextImageIndex]);

        // Set initial positions for sliding effect
        //currentImageView.setTranslateX(0);
        nextImageView.setTranslateX(800);

        // Create the sliding animation
        Timeline slideTimeline = new Timeline(
                new KeyFrame(Duration.ZERO,
                        new KeyValue(currentImageView.translateXProperty(), 0),
                        new KeyValue(nextImageView.translateXProperty(),1700)),
                new KeyFrame(Duration.seconds(1),
                        new KeyValue(currentImageView.translateXProperty(), -1700),
                        new KeyValue(nextImageView.translateXProperty(), 0)));

        slideTimeline.setOnFinished(event -> {
            // Update the current image index and swap the ImageViews
            currentImageIndex = nextImageIndex;
            ImageView temp = currentImageView;
            currentImageView = nextImageView;
            nextImageView = temp;
        });

        slideTimeline.play();
    }
    private void loadImages() {
        // Replace with the paths to your images
        String[] imagePaths = {
                "file:dogodiary\\src\\main\\resources\\images\\tran1.png",
                "file:dogodiary\\src\\main\\resources\\images\\tran2.png",
                "file:dogodiary\\src\\main\\resources\\images\\tran3.png",
                "file:dogodiary\\src\\main\\resources\\images\\tran4.png"
                // "file:dogodiary\\src\\main\\resources\\images\\peak.jpg",
                // Add more image paths as needed
        };

        images = new Image[imagePaths.length];
        for (int i = 0; i < imagePaths.length; i++) {
            images[i] = new Image(imagePaths[i]+"\t");
        }
    }

}
