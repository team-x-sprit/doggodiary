package com.project;
import com.twilio.Twilio;
import com.twilio.rest.verify.v2.service.Verification;

public class Example {
  // Find your Account Sid and Token at twilio.com/console
  public static final String ACCOUNT_SID = "ACab89016702584eae2dc56e15448ed902";
  public static final String AUTH_TOKEN = "adefd75bbee7a58f921fbf29d0b03f64";

  public static void main(String[] args) {
    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    Verification verification = Verification.creator(
            "VA3c430f5ff5d13d7c3a4fc8629237c4b4",
            "+919322735151",
            "sms")
        .create();

    System.out.println(verification.getSid());
  }
}



