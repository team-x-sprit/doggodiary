package com.project.controller;

import java.io.FileInputStream;
import java.io.IOException;

import com.project.Adoption.FirstPage;
import com.project.fireBase.firebaseService;
import com.project.shop.accesories;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

import javafx.animation.TranslateTransition;
import javafx.application.Application;

import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

public class LoginPage extends Application {

    private Stage primaryStage;
    private firebaseService firebaseService;
    private Scene scene;
    private Pane container;
    private VBox loginBox, signupBox;

    @Override
    public void start(Stage primaryStage) throws Exception {

        this.primaryStage = primaryStage;

        try {
            FileInputStream serviceAccount = new FileInputStream("dogodiary/src/main/resources/fx-auth-fb.json");

            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setDatabaseUrl("https://fx-auth-fb-743aa-default-rtdb.asia-southeast1.firebasedatabase.app/")
                    .build();
            FirebaseApp.initializeApp(options);
        } catch (IOException e) {

            e.printStackTrace();
        }
        Scene scene = initializeLoginScene(primaryStage);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public Scene initializeLoginScene(Stage primaryStage) {

        Image dogoImage = new Image("images/logo.jpg");
        ImageView dogoImageView = new ImageView(dogoImage);
        dogoImageView.setFitHeight(60);
        dogoImageView.setFitWidth(70);
        Button home = new Button("DOGGODIARY.COM");
        home.setTextFill(Color.GREENYELLOW);
        home.setAlignment(Pos.BASELINE_LEFT);
        home.setFont(new Font("Impact", 25));
        home.setMaxHeight(80);
        home.setPrefWidth(350);
        home.setStyle("-fx-background-color:white");
        home.setStyle("-fx-border-color:black");
        // home.setEffect(new DropShadow(20, Color.WHITE));
        home.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
        home.setGraphic(dogoImageView);

        Button homeService = new Button("HOME");
        homeService.setTextFill(Color.GREENYELLOW);
        homeService.setAlignment(Pos.CENTER);
        homeService.setFont(new Font("Serif Regular", 20));
        homeService.setPrefHeight(80);
        homeService.setPrefWidth(190);
        homeService.setStyle("-fx-background-color:white");
        homeService.setStyle("-fx-border-color:black");
        homeService.setEffect(new DropShadow(20, Color.BLACK));
        homeService.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

        Button availPuppy = new Button("AVAILABLE \nPUPPIES");
        availPuppy.setTextFill(Color.GREENYELLOW);
        availPuppy.setAlignment(Pos.CENTER);
        availPuppy.setFont(new Font("Serif Regular", 20));
        availPuppy.setPrefHeight(80);
        availPuppy.setPrefWidth(150);
        availPuppy.setStyle("-fx-background-color:white");
        availPuppy.setStyle("-fx-border-color:black");
        availPuppy.setEffect(new DropShadow(20, Color.BLACK));
        availPuppy.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

        Button adopttion = new Button("ADOPTION");
        adopttion.setTextFill(Color.GREENYELLOW);
        adopttion.setAlignment(Pos.CENTER);
        adopttion.setFont(new Font("Serif Regular", 20));
        adopttion.setPrefHeight(80);
        adopttion.setPrefWidth(150);
        adopttion.setStyle("-fx-background-color:white");
        adopttion.setStyle("-fx-border-color:black");
        adopttion.setEffect(new DropShadow(20, Color.BLACK));
        adopttion.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
        adopttion.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Scene scene = new FirstPage().CreateScene(primaryStage);
                primaryStage.setScene(scene);

            }
        });

        Image shopImage = new Image("images/shop.jpg");
        ImageView cartView = new ImageView(shopImage);
        cartView.setFitHeight(60);
        cartView.setFitWidth(70);
        Button shop = new Button("SHOP");
        shop.setTextFill(Color.GREENYELLOW);
        shop.setAlignment(Pos.CENTER);
        shop.setFont(new Font("Serif Regular", 20));
        shop.setPrefHeight(80);
        shop.setPrefWidth(190);
        shop.setStyle("-fx-border-color:black");
        // shop.setStyle("-fx-background-color:white");
        shop.setEffect(new DropShadow(20, Color.BLACK));
        shop.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
        shop.setGraphic(cartView);
        shop.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                Scene scene = new accesories().CreateScene(primaryStage);
                primaryStage.setScene(scene);
                
                }
            });

        Button login = new Button("LOGIN");
        login.setTextFill(Color.GREENYELLOW);
        login.setAlignment(Pos.CENTER);
        login.setFont(new Font("serif Regular", 20));
        login.setPrefHeight(80);
        login.setPrefWidth(150);
        // login.setStyle("-fx-background-color:yellow");
        login.setStyle("-fx-border-color:black");
        login.setEffect(new DropShadow(20, Color.BLACK));
        login.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

        Button about = new Button("ABOUT");
        about.setTextFill(Color.GREENYELLOW);
        about.setAlignment(Pos.BASELINE_LEFT);
        about.setFont(new Font("serif regular", 20));
        about.setMaxHeight(80);
        about.setPrefWidth(150);
        about.setStyle("-fx-background-color:white");
        about.setStyle("-fx-border-color:black");
        about.setEffect(new DropShadow(20, Color.BLACK));
        about.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

        HBox hb1 = new HBox(home, homeService, availPuppy, adopttion, shop, login, about);
        hb1.setPrefHeight(80);
        hb1.setPrefWidth(1980);

        container = new Pane();

        // Create login and signup scenes
        loginBox = createLoginBox();
        signupBox = createSignupBox();

        // Position the signupBox off-screen to the right
        signupBox.setTranslateX(1500);

        // Add both boxes to the container
        container.getChildren().addAll(loginBox, signupBox);
        HBox seBox = new HBox(container);
        //seBox.setLayoutX(500);
        seBox.setAlignment(Pos.CENTER_LEFT);

        VBox setBox = new VBox(70, hb1, seBox);

        scene = new Scene(setBox, 1980, 1080);
        primaryStage.setTitle("Firebase Auth Example");
        primaryStage.setScene(scene);
        primaryStage.show();
        return scene;
    }

    private VBox createLoginBox() {
        TextField emailField = new TextField();
        emailField.setMaxWidth(250);
        emailField.setPromptText("Email");
        emailField.setStyle("-fx-border-color:black");

        PasswordField passwordField = new PasswordField();
        passwordField.setMaxWidth(250);
        passwordField.setPromptText("Password");
        passwordField.setStyle("-fx-border-color:black");

        Button loginButton = new Button("Log In");
        loginButton.setTextFill(Color.WHITE);
        loginButton.setPrefWidth(100);
        loginButton.setPrefHeight(30);
        loginButton.setStyle("-fx-background-color:orangered");

        Button switchToSignupButton = new Button("Sign Up");
        switchToSignupButton.setTextFill(Color.WHITE);
        switchToSignupButton.setPrefWidth(100);
        switchToSignupButton.setPrefHeight(30);
        switchToSignupButton.setStyle("-fx-background-color:orangered");

        firebaseService = new firebaseService(this, emailField, passwordField);
        loginButton.setOnAction(event -> firebaseService.login());

        switchToSignupButton.setOnAction(event -> switchToSignup());

        Label lb4 = new Label("EMAIL:");
        lb4.setTextFill(Color.BLACK);
        lb4.setFont(new Font(20));
        Label lb2 = new Label("PASSWORD:");
        lb2.setTextFill(Color.BLACK);
        lb2.setFont(new Font(20));

        VBox fieldBox = new VBox(10, lb4, emailField, lb2, passwordField);
        HBox hb4 = new HBox(fieldBox);
        hb4.setAlignment(Pos.CENTER);
        HBox buttonBox = new HBox(20, loginButton, switchToSignupButton);
        buttonBox.setAlignment(Pos.BASELINE_CENTER);
        VBox comBox = new VBox(20, hb4, buttonBox);
        comBox.setLayoutX(50);
        comBox.setLayoutY(200);
        comBox.setPrefHeight(500);
        comBox.setPrefWidth(450);
        comBox.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));

        Image ig3 = new Image("images/log.jpg");
        ImageView iv3 = new ImageView(ig3);
        iv3.setFitHeight(500);
        iv3.setFitWidth(450);
        HBox hb2 = new HBox(iv3);
        hb2.setAlignment(Pos.CENTER);
        VBox vb1 = new VBox(hb2);
        vb1.setPrefHeight(500);
        vb1.setPrefWidth(450);

        HBox hb = new HBox(vb1, comBox);
        hb.setLayoutX(180);
        hb.setLayoutY(150);

        VBox loginBox = new VBox(hb);
        loginBox.setStyle("-fx-background-color:LIME");
        return loginBox;
    }

    private VBox createSignupBox() {
        TextField emailField = new TextField();
        emailField.setMaxWidth(250);
        emailField.setPromptText("Email");
        emailField.setStyle("-fx-border-color:black");

        PasswordField passwordField = new PasswordField();
        passwordField.setMaxWidth(250);
        passwordField.setPromptText("Password");
        passwordField.setStyle("-fx-border-color:black");

        TextField mobileField = new TextField();
        mobileField.setMaxWidth(250);
        mobileField.setPromptText("Mobile Number");
        mobileField.setStyle("-fx-border-color:black");

        Button signUpButton = new Button("Sign Up");
        signUpButton.setTextFill(Color.WHITE);
        signUpButton.setPrefWidth(100);
        signUpButton.setPrefHeight(30);
        signUpButton.setStyle("-fx-background-color:orangered");

        Button switchToLoginButton = new Button("Log In");
        switchToLoginButton.setTextFill(Color.WHITE);
        switchToLoginButton.setPrefWidth(100);
        switchToLoginButton.setPrefHeight(30);
        switchToLoginButton.setStyle("-fx-background-color:orangered");

        firebaseService = new firebaseService(this, emailField, passwordField);
        signUpButton.setOnAction(event -> firebaseService.signUp());

        switchToLoginButton.setOnAction(event -> switchToLogin());

        Label lb4 = new Label("EMAIL:");
        lb4.setTextFill(Color.BLACK);
        lb4.setFont(new Font(20));
        Label lb2 = new Label("PASSWORD:");
        lb2.setTextFill(Color.BLACK);
        lb2.setFont(new Font(20));
        Label lb3 = new Label("MOBILE NUMBER:");
        lb3.setTextFill(Color.BLACK);
        lb3.setFont(new Font(20));

        VBox fieldBox = new VBox(10, lb4, emailField, lb2, passwordField, lb3, mobileField);
        HBox hb4 = new HBox(fieldBox);
        hb4.setAlignment(Pos.CENTER);
        HBox buttonBox = new HBox(20, signUpButton, switchToLoginButton);
        buttonBox.setAlignment(Pos.BASELINE_CENTER);
        VBox comBox = new VBox(20, hb4, buttonBox);
        comBox.setLayoutX(50);
        comBox.setLayoutY(200);
        comBox.setPrefHeight(500);
        comBox.setPrefWidth(450);
        comBox.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        Image ig3 = new Image("images/sign.jpg");
        ImageView iv3 = new ImageView(ig3);
        iv3.setFitHeight(500);
        iv3.setFitWidth(450);
        HBox hb2 = new HBox(iv3);
        hb2.setAlignment(Pos.CENTER);
        VBox vb1 = new VBox(hb2);
        vb1.setPrefHeight(500);
        vb1.setPrefWidth(450);

        HBox hb = new HBox(comBox, vb1);
        hb.setLayoutX(180);
        hb.setLayoutY(150);

        VBox signupBox = new VBox(hb);
        signupBox.setStyle("-fx-background-color:LIME");
        return signupBox;
    }

    private void switchToSignup() {
        TranslateTransition transition1 = new TranslateTransition(Duration.seconds(0.5), loginBox);
        transition1.setToX(-1500);
        TranslateTransition transition2 = new TranslateTransition(Duration.seconds(0.5), signupBox);
        transition2.setToX(0);

        transition1.play();
        transition2.play();
    }

    private void switchToLogin() {
        TranslateTransition transition1 = new TranslateTransition(Duration.seconds(0.5), loginBox);
        transition1.setToX(0);
        TranslateTransition transition2 = new TranslateTransition(Duration.seconds(0.5), signupBox);
        transition2.setToX(1500);

        transition1.play();
        transition2.play();
    }

    public void setPrimaryStageScene(Scene scene) {
        primaryStage.setScene(scene);
    }
}
