package com.project.Adoption;

import com.project.controller.LoginPage;
import com.project.shop.accesories;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class FirstPage extends Application {


    public Scene CreateScene(Stage primaryStage){ 
        primaryStage.setTitle("adoption");
        Image dogoImage = new Image("images/logo.jpg");
        ImageView dogoImageView = new ImageView(dogoImage);
        dogoImageView.setFitHeight(60);
        dogoImageView.setFitWidth(70);
        Button home = new Button("doggodiary");
        home.setTextFill(Color.ORANGERED);
        home.setAlignment(Pos.BASELINE_LEFT);
        home.setFont(new Font("elephant",25));
        home.setMaxHeight(80);
        home.setPrefWidth(350);
        home.setStyle("-fx-background-color:white");
        home.setStyle("-fx-border-color:black");
        home.setEffect(new DropShadow(20, Color.WHITE));
       home.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
       home.setGraphic(dogoImageView);
        
        Button homeService = new Button("HOME");
        homeService.setTextFill(Color.GREENYELLOW);
        homeService.setAlignment(Pos.CENTER);
        homeService.setFont(new Font("Serif Regular",20));
        homeService.setPrefHeight(80);
        homeService.setPrefWidth(190);
        homeService.setStyle("-fx-background-color:white");
        homeService.setStyle("-fx-border-color:black");
        homeService.setEffect(new DropShadow(20, Color.BLACK));
       homeService.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

       Button availPuppy = new Button("AVAILABLE \nPUPPIES");
       availPuppy.setTextFill(Color.GREENYELLOW);
       availPuppy.setAlignment(Pos.CENTER);
       availPuppy.setFont(new Font("Serif Regular",20));
       availPuppy.setPrefHeight(80);
       availPuppy.setPrefWidth(150);
       availPuppy.setStyle("-fx-background-color:white");
       availPuppy.setStyle("-fx-border-color:black");
       availPuppy.setEffect(new DropShadow(20, Color.BLACK));
      availPuppy.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

      Button adopttion = new Button("ADOPTION");
      adopttion.setTextFill(Color.WHITE);
      adopttion.setAlignment(Pos.CENTER);
      adopttion.setFont(new Font("Serif Regular",20));
      adopttion.setPrefHeight(80);
      adopttion.setPrefWidth(150);
      adopttion.setStyle("-fx-background-color:white");
      adopttion.setStyle("-fx-border-color:black");
      adopttion.setEffect(new DropShadow(20, Color.BLACK));
      adopttion.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

       
        
        Image shopImage = new Image("images/shop.jpg");
        ImageView cartView = new ImageView(shopImage);
        cartView.setFitHeight(60);
        cartView.setFitWidth(70);
        Button shop = new Button("SHOP");
        shop.setTextFill(Color.GREENYELLOW);
        shop.setAlignment(Pos.CENTER);
        shop.setFont(new Font("Serif Regular",20));  
        shop.setPrefHeight(80);
        shop.setPrefWidth(190); 
        shop.setStyle("-fx-border-color:black");
       // shop.setStyle("-fx-background-color:white");
        shop.setEffect(new DropShadow(20, Color.BLACK));
        shop.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
        shop.setGraphic(cartView);
        shop.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                Scene scene = new accesories().CreateScene(primaryStage);
                primaryStage.setScene(scene);
                
                }
            });
           
        Button login = new Button("LOGIN");
        login.setTextFill(Color.GREENYELLOW);
        login.setAlignment(Pos.CENTER);
        login.setFont(new Font("serif Regular",20));
        login.setPrefHeight(80);
        login.setPrefWidth(150);
       // login.setStyle("-fx-background-color:yellow");
        login.setStyle("-fx-border-color:black");
        login.setEffect(new DropShadow(20, Color.BLACK));
       login.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

       login.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            
            Scene scene = new LoginPage().initializeLoginScene(primaryStage);
            primaryStage.setScene(scene);
            
            }
        });
        Button about = new Button("ABOUT");
        about.setTextFill(Color.GREENYELLOW);
        about.setAlignment(Pos.BASELINE_LEFT);
        about.setFont(new Font("serif regular",20));
        about.setMaxHeight(80);
        about.setPrefWidth(150);
        about.setStyle("-fx-background-color:white");
        about.setStyle("-fx-border-color:black");
        about.setEffect(new DropShadow(20, Color.BLACK));
        about.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
        
        
      
      
        HBox hb1 = new HBox(home,homeService,availPuppy,adopttion,shop,login,about);
        hb1.setPrefHeight(80);
        hb1.setPrefWidth(1980);

        Label lb1 = new Label("RE-HOME AND ADOPT THE PET IN INDIA");
        lb1.setFont(new Font("Cambria Math",50));
        lb1.setFont(new Font(30));
        Label lb2 = new Label("Every Pet Deserves a Good Home. #Adoptlove");
        lb2.setFont(new Font(10));
        Button adopt = new Button("ADOPT A DOG");
        adopt.setTextFill(Color.NAVY);
        adopt.setAlignment(Pos.CENTER);
        adopt.setFont(new Font(20));  
        adopt.setPrefHeight(20);
        adopt.setPrefWidth(200); 
       // shop.setStyle("-fx-border-color:black");
        adopt.setStyle("-fx-background-color:orange");
        //adopt.setEffect(new DropShadow(20, Color.BLACK));

        adopt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Scene scene = new secondPage().CreateScene(primaryStage);
                primaryStage.setScene(scene);
            }
        });
        
        VBox vBox =new VBox(10,lb1,lb2,adopt);
        vBox.setLayoutX(500);
        vBox.setLayoutY(500);
        vBox.setPrefHeight(200);
        vBox.setPrefWidth(1000);
        vBox.setAlignment(Pos.CENTER);
        
        HBox hb2 = new HBox(vBox);
        hb2.setLayoutX(150);
        hb2.setLayoutY(500);
        hb2.setPrefHeight(200);
        hb2.setPrefWidth(1000);
        hb2.setStyle("-fx-background-color:white");
        
        Pane viewPane1 = new Pane(hb1,hb2);
        
        

       // viewPane.setStyle("-fx-background-color:black");
         BackgroundImage myBI= new BackgroundImage(new Image("images/g.jpg",1280,750,false,true),
        BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
        BackgroundSize.DEFAULT);       //then you set to your node*/
        viewPane1.setBackground(new Background(myBI));

        Label lb3 = new Label("YOUR DOG ADOPTION JOURNEY WITH DOGGODIARY");
        lb3.setFont(new Font("Impact",40));
        lb3.setTextFill(Color.BLACK);
        lb3.setAlignment(Pos.CENTER);
        //lb3.setFont(new Font(40));
        lb3.setLayoutX(230);
        lb3.setLayoutY(30);

        Image ig1 = new Image("images/n.jpg");
        ImageView iv1 = new ImageView(ig1);
        iv1.setFitHeight(500);
        iv1.setFitWidth(500);
        //iv2.setPreserveRatio(true);
        HBox hb3 = new HBox(iv1);
        hb3.setLayoutX(100);
        hb3.setLayoutY(200);
        hb3.setPrefHeight(500);
        hb3.setPrefWidth(500);

        Label txt1 = new Label("Connect");
        txt1.setTextFill(Color.ORANGERED);
        txt1.setFont(new Font(30));   
        Label lb4  = new Label("Once you find a pet, click show number to get contact \ninfo for their pet parent or rescue. Contact them to learn\n more about how to meet and adopt the pet.");
        VBox v1 = new VBox(txt1,lb4);
        v1.setPrefWidth(500);
        //v1.setAlignment(Pos.CENTER);
        Image ig2 = new Image("images/m.png");
        ImageView iv2 = new ImageView(ig2);
        iv2.setFitHeight(165);
        iv2.setFitWidth(150);
     // iv.setPreserveRatio(true);
        HBox  hb4 = new HBox(iv2,v1);
        hb4.setPrefWidth(400);
        hb4.setPrefHeight(165);
        hb4.setStyle("-fx-background-color:white");

        Label txt2 = new Label("ADOPTLOVE");
        txt2.setTextFill(Color.ORANGERED);
        txt2.setFont(new Font(30));   
        Label lb5  = new Label("The rescue or pet parents will walk you through their\n adoption process. Prepare your home for the arrival of\n your fur baby to help them adjust to their new family.");
        VBox v2 = new VBox(txt2,lb5);
        v2.setPrefWidth(500);
        //v1.setAlignment(Pos.CENTER);
        Image ig3 = new Image("images/k.jpg");
        ImageView iv3 = new ImageView(ig3);
        iv3.setFitHeight(165);
        iv3.setFitWidth(150);
     // iv.setPreserveRatio(true);
        HBox  hb5 = new HBox(iv3,v2);
        hb5.setPrefWidth(400);
        hb5.setPrefHeight(165);
        hb5.setStyle("-fx-background-color:white");

        Label txt3 = new Label("FREE VET CONSULTATION");
        txt3.setTextFill(Color.ORANGERED);
        txt3.setFont(new Font(30));   
        Label lb6  = new Label("DoggoDiary will help your dog to settle down in its new\n home, once you complete the Adoption journey reach out \nto us for free vet consultation");
        VBox v3 = new VBox(txt3,lb6);
        v3.setPrefWidth(500);
        //v1.setAlignment(Pos.CENTER);
        Image ig4 = new Image("images/i.png");
        ImageView iv4 = new ImageView(ig4);
        iv4.setFitHeight(165);
        iv4.setFitWidth(150);
     // iv.setPreserveRatio(true)
        HBox  hb6 = new HBox(iv4,v3);
        hb6.setPrefWidth(500);
        hb6.setPrefHeight(165);
        hb6.setStyle("-fx-background-color:white");


        VBox vBox2 = new VBox(hb4,hb5,hb6);
        vBox2.setLayoutX(700);
        vBox2.setLayoutY(150);


        Pane viewPane2 = new Pane(lb3,hb3,vBox2);
        
       // viewPane.setStyle("-fx-background-color:black");
        BackgroundImage myBI2= new BackgroundImage(new Image("images/n.jpg",1280,750,false,true),
        BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
        BackgroundSize.DEFAULT);

        viewPane2.setStyle("-fx-background-color:white");

        VBox vBox4 = new VBox(viewPane1,viewPane2);
        //vBox4.setAlignment(Pos.TOP_CENTER);
        for(int i =0;i<2;i++){
            vBox4.getChildren().add(new Text(""));

        }
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(vBox4);
       // scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);

        Scene scene = new Scene(scrollPane,primaryStage.getWidth(),primaryStage.getHeight());
        return scene;

    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("adoption");
        Scene scene = CreateScene(primaryStage);
        primaryStage.setScene(scene);
        primaryStage.show();  
    }

}
