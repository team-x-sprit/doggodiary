package com.project.Adoption;

import com.project.controller.LoginPage;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class secondPage extends Application{

    public Scene CreateScene(Stage primaryStage){
        primaryStage.setTitle("adoption");
    
        Image dogoImage = new Image("images/logo.jpg");
        ImageView dogoImageView = new ImageView(dogoImage);
        dogoImageView.setFitHeight(60);
        dogoImageView.setFitWidth(70);
        Button home = new Button("DOGGODIARY.COM");
        home.setTextFill(Color.GREENYELLOW);
        home.setAlignment(Pos.BASELINE_LEFT);
        home.setFont(new Font("Impact",25));
        home.setMaxHeight(80);
        home.setPrefWidth(350);
        home.setStyle("-fx-background-color:white");
        home.setStyle("-fx-border-color:black");
        home.setEffect(new DropShadow(20, Color.BLACK));
       home.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
       home.setGraphic(dogoImageView);
        
        Button homeService = new Button("HOME");
        homeService.setTextFill(Color.GREENYELLOW);
        homeService.setAlignment(Pos.CENTER);
        homeService.setFont(new Font("Serif Regular",20));
        homeService.setPrefHeight(80);
        homeService.setPrefWidth(190);
        homeService.setStyle("-fx-background-color:white");
        homeService.setStyle("-fx-border-color:black");
        homeService.setEffect(new DropShadow(20, Color.BLACK));
       homeService.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

       Button availPuppy = new Button("AVAILABLE \nPUPPIES");
       availPuppy.setTextFill(Color.GREENYELLOW);
       availPuppy.setAlignment(Pos.CENTER);
       availPuppy.setFont(new Font("Serif Regular",20));
       availPuppy.setPrefHeight(80);
       availPuppy.setPrefWidth(150);
       availPuppy.setStyle("-fx-background-color:white");
       availPuppy.setStyle("-fx-border-color:black");
       availPuppy.setEffect(new DropShadow(20, Color.BLACK));
      availPuppy.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

      Button adopttion = new Button("ADOPTION");
      adopttion.setTextFill(Color.GREENYELLOW);
      adopttion.setAlignment(Pos.CENTER);
      adopttion.setFont(new Font("Serif Regular",20));
      adopttion.setPrefHeight(80);
      adopttion.setPrefWidth(150);
      adopttion.setStyle("-fx-background-color:white");
      adopttion.setStyle("-fx-border-color:black");
      adopttion.setEffect(new DropShadow(20, Color.BLACK));
      adopttion.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

       
        
        Image shopImage = new Image("images/shop.jpg");
        ImageView cartView = new ImageView(shopImage);
        cartView.setFitHeight(60);
        cartView.setFitWidth(70);
        Button shop = new Button("SHOP");
        shop.setTextFill(Color.GREENYELLOW);
        shop.setAlignment(Pos.CENTER);
        shop.setFont(new Font("Serif Regular",20));  
        shop.setPrefHeight(80);
        shop.setPrefWidth(190); 
        shop.setStyle("-fx-border-color:black");
       // shop.setStyle("-fx-background-color:white");
        shop.setEffect(new DropShadow(20, Color.BLACK));
        shop.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
        shop.setGraphic(cartView);
        
       
        
        Button login = new Button("LOGIN");
        login.setTextFill(Color.GREENYELLOW);
        login.setAlignment(Pos.CENTER);
        login.setFont(new Font("serif Regular",20));
        login.setPrefHeight(80);
        login.setPrefWidth(150);
       // login.setStyle("-fx-background-color:yellow");
        login.setStyle("-fx-border-color:black");
        login.setEffect(new DropShadow(20, Color.BLACK));
       login.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

       login.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            Scene scene = new LoginPage().createLogiScene(primaryStage);
            primaryStage.setScene(scene);
            
            }
        });
        Button about = new Button("ABOUT");
        about.setTextFill(Color.GREENYELLOW);
        about.setAlignment(Pos.BASELINE_LEFT);
        about.setFont(new Font("serif regular",20));
        about.setMaxHeight(80);
        about.setPrefWidth(150);
        about.setStyle("-fx-background-color:white");
        about.setStyle("-fx-border-color:black");
        about.setEffect(new DropShadow(20, Color.BLACK));
        about.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
        
        
      
      
        HBox hb1 = new HBox(home,homeService,availPuppy,adopttion,shop,login,about);
        hb1.setPrefHeight(80);
        hb1.setPrefWidth(1980);
        

       Button back = new Button("back");
       back.setTextFill(Color.BLACK);
        back.setLayoutY(80);
        back.setFont(new Font("Serif Regular",20));
        back.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
       back.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            Scene scene = new FirstPage().CreateScene(primaryStage);
            primaryStage.setScene(scene);
            
            }
        });
        
        Label label = new Label("Dogs Available For Adoption");
        label.setFont(new Font("Cambria Math",50));
        label.setTextFill(Color.ROYALBLUE);
        label.setAlignment(Pos.CENTER);
        label.setLayoutX(340);
        label.setLayoutY(90);

        Image ig1 = new Image("images/ad1.jpeg");
        ImageView iv1 = new ImageView(ig1);
        iv1.setFitHeight(230);
        iv1.setFitWidth(285);
       // iv1.setPreserveRatio(true);
        HBox hb2 = new HBox(iv1);
        hb2.setAlignment(Pos.CENTER);
        Label txt1 = new Label("DOLLY");
        txt1.setTextFill(Color.RED);
        txt1.setFont(new Font("Impact",30));   
        Label lb1  = new Label("Female,Senior \n" + //
                    "Nashik,Maharastra\n" + //
                    "Contact Details\n" + //
                    "     Name: Sakshi\n" + //
                    "     Mob No:8465465985");
        lb1.setFont(new Font("Century",15));
        lb1.setTextFill(Color.NAVY);
        HBox line1 = new HBox(lb1);
        line1.setBackground(new Background(new BackgroundFill(Color.GREENYELLOW , CornerRadii.EMPTY, Insets.EMPTY)));
        line1.setPrefHeight(150);
        line1.setStyle("-fx-border-color:black");
        VBox v1 = new VBox(hb2,txt1,line1);
        v1.setPrefWidth(285);
        v1.setPrefHeight(150);
        v1.setStyle("-fx-border-color:black");
        //v1.setBackground(new Background(new BackgroundFill(Color.RED , CornerRadii.EMPTY, Insets.EMPTY)));
      


        Image ig2 = new Image("images/a.jpeg");
        ImageView iv2 = new ImageView(ig2);
        iv2.setLayoutX(50);
        iv2.setFitHeight(230);
        iv2.setFitWidth(285);
        HBox hb3 = new HBox(iv2);
        hb3.setAlignment(Pos.CENTER);
        Label txt2 = new Label("TOMMY");
        txt2.setTextFill(Color.RED);
        txt2.setFont(new Font("impact",30));   
        Label lb2  = new Label("Male,Senior \n" + //
                    "Mumbai,Maharastra\n" + //
                    "Contact Details\n" + //
                    "     Name: Vedant\n" + //
                    "     Mob No:9965465985");
        lb2.setFont(new Font("Century",15));
        lb2.setTextFill(Color.BLACK);
        HBox line2 = new HBox(lb2);
        line2.setBackground(new Background(new BackgroundFill(Color.GREENYELLOW , CornerRadii.EMPTY, Insets.EMPTY)));
        line2.setPrefHeight(150);
        line2.setStyle("-fx-border-color:black");
        VBox v2 = new VBox(hb3,txt2,line2);
        v2.setPrefWidth(285);
        v2.setPrefHeight(150);
        v2.setStyle("-fx-border-color:black");
        //v2.setBackground(new Background(new BackgroundFill(Color.YELLOW , CornerRadii.EMPTY, Insets.EMPTY)));
        
      


        Image ig3 = new Image("images/ad2.jpeg");
        ImageView iv3 = new ImageView(ig3);
        iv3.setFitHeight(230);
        iv3.setFitWidth(285);
        HBox hb = new HBox(iv3);
        hb.setAlignment(Pos.CENTER);
        Label txt3 = new Label("MARSHALL");
        txt3.setTextFill(Color.RED);
        txt3.setFont(new Font("Impact",30));   
        Label lb3  = new Label("Male,Senior \n" +//
                    "Pune,Maharastra\n" +//
                    "Contact Details\n" + //
                    "     Name: Dhiraj\n" + //
                    "     Mob No:9545445985");
        lb3.setFont(new Font("Century",15));
        lb3.setTextFill(Color.BLACK);
        HBox line3 = new HBox(lb3);
        line3.setBackground(new Background(new BackgroundFill(Color.GREENYELLOW , CornerRadii.EMPTY, Insets.EMPTY)));
        line3.setPrefHeight(150);
        line3.setStyle("-fx-border-color:black");
        VBox v3 = new VBox(hb,txt3,line3);
        v3.setPrefWidth(285);
        v3.setPrefHeight(150);
        v3.setStyle("-fx-border-color:black");
       // v3.setBackground(new Background(new BackgroundFill(Color.CHARTREUSE , CornerRadii.EMPTY, Insets.EMPTY)));
      


        Image ig4 = new Image("images/u.jpeg");
        ImageView iv4 = new ImageView(ig4);
        iv4.setFitHeight(230);
        iv4.setFitWidth(285);
        HBox hb4 = new HBox(iv4);
        hb4.setAlignment(Pos.CENTER);
        Label txt4 = new Label("MEENU");
        txt4.setTextFill(Color.RED);
        txt4.setFont(new Font("Impact",30));  
        Label lb4  = new Label("Female,Senior \nNashik,Maharastra\nContact Details\n     Name: Shiv\n     Mob No:7565465985");
        lb4.setFont(new Font("Century",15));
        HBox line4 = new HBox(lb4);
        line4.setBackground(new Background(new BackgroundFill(Color.GREENYELLOW , CornerRadii.EMPTY, Insets.EMPTY)));
        line4.setPrefHeight(150);
        line4.setStyle("-fx-border-color:black");
        lb4.setTextFill(Color.BLACK);
        VBox v4 = new VBox(hb4,txt4,line4);
        v4.setPrefWidth(285);
        v4.setPrefHeight(150);
        v4.setStyle("-fx-border-color:black");
        //v4.setBackground(new Background(new BackgroundFill(Color.DEE , CornerRadii.EMPTY, Insets.EMPTY)));
      


        //v1.setAlignment(Pos.CENTER)
        HBox  hb5 = new HBox(10,v1,v2,v3,v4);
        hb5.setLayoutY(250);
        hb5.setLayoutX(60);
        hb5.setPrefWidth(1100);
        hb5.setPrefHeight(400);
        hb5.setStyle("-fx-background-color:white");

        Pane viewPane = new Pane(hb1,label,hb5,back);
        viewPane.setStyle("-fx-background-color:white");
        BackgroundImage myBI2= new BackgroundImage(new Image("images/peak.jpg",1280,750,false,true),
        BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
        BackgroundSize.DEFAULT);
       // viewPane.setBackground(new Background(myBI2));

        Scene scene = new Scene(viewPane,1980,1080);
        return scene;

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("adoption");
        Scene scene = CreateScene(primaryStage);
        primaryStage.setScene(scene);
        primaryStage.show();
        
        
    }

    
}
