package com.project.prem;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.awt.Desktop;
import java.net.URI;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class prem extends Application {

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("DoggoDiary");

        // Load images
        Image image1 = new Image("file:dogodiary\\src\\main\\resources\\images\\mylady4.png");
        Image image2 = new Image("file:dogodiary/src/main/resources/images/page2.png");
        Image image3 = new Image("file:dogodiary/src/main/resources/images/page3.png");
        Image additionalImage = new Image("file:dogodiary/src/main/resources/images/peakpx (4).jpg");

        Image socialImage1 = new Image("file:dogodiary\\src\\main\\resources\\images\\file (4).png");
        Image socialImage2 = new Image("file:dogodiary\\src\\main\\resources\\images\\file (7).png");
        Image socialImage3 = new Image("file:dogodiary\\src\\main\\resources\\images\\insta logo.png");

        // Create ImageViews
        ImageView imageView1 = new ImageView(image1);
        imageView1.setFitHeight(1080);
        imageView1.setFitWidth(1980);
        ImageView imageView2 = new ImageView(image2);
        ImageView imageView3 = new ImageView(image3);
        ImageView additionalImageView = new ImageView(additionalImage);

        ImageView socialImageView1 = new ImageView(socialImage1);
        ImageView socialImageView2 = new ImageView(socialImage2);
        ImageView socialImageView3 = new ImageView(socialImage3);

        // Set ImageViews to fit the width of the window
        imageView1.setFitWidth(1900);
        
        imageView2.setFitWidth(1900);
        imageView3.setFitWidth(1900);
        additionalImageView.setFitWidth(987); // Adjust width as needed

        //imageView1.setPreserveRatio(true);
        imageView2.setPreserveRatio(true);
        imageView3.setPreserveRatio(true);
        additionalImageView.setPreserveRatio(true);

        socialImageView1.setFitWidth(50);
        socialImageView1.setPreserveRatio(true);

        socialImageView2.setFitWidth(50);
        socialImageView2.setPreserveRatio(true);

        socialImageView3.setFitWidth(45);
        socialImageView3.setPreserveRatio(true);

        additionalImageView.setTranslateY(-42);
        socialImageView1.setTranslateY(-5);
        socialImageView2.setTranslateY(3);
        socialImageView3.setTranslateY(-3);

        // Create buttons
        Button doggoDiaryButton = new Button("doggodiary");
        doggoDiaryButton.setPadding(new Insets(80,1080, 180, 0));
        doggoDiaryButton.setStyle("-fx-font-size: 75px; " +
                "-fx-font-family: 'Elephant'; " +
                "-fx-background-color: transparent; " +
                "-fx-border-color: transparent; " +
                "-fx-text-fill: #f2a30a;");

        Button homeButton = new Button("Home");
        Button availablePuppiesButton = new Button("Available Puppies");
        Button adoptionButton = new Button("Adoption");
        Button loginButton = new Button("Login");
        Button aboutUsButton = new Button("About Us");

        // Style the buttons
        String buttonStyle = "-fx-font-size: 20px; " +
                "-fx-background-color: transparent; " +
                "-fx-border-color: transparent; " +
                "-fx-text-fill: #f2a30a;";
        homeButton.setStyle(buttonStyle);
        availablePuppiesButton.setStyle(buttonStyle);
        adoptionButton.setStyle(buttonStyle);
        loginButton.setStyle(buttonStyle);
        aboutUsButton.setStyle(buttonStyle);

        // Create the "We are social" label and style it
        Label socialLabel = new Label("We are social");
        socialLabel.setStyle("-fx-font-size: 30px; " +
                "-fx-font-family: 'Century'; " +
                "-fx-text-fill: white;");
        socialLabel.setTranslateX(-200);
        socialLabel.setTranslateY(20);

        // Create labels and button for image1
        Label label1 = new Label("'Happiness comes\nin four legs, has a\nfurry body and\njoyful wagging tail'");
        label1.setStyle("-fx-font-size: 60px; " +
                "-fx-font-family: 'Cambria'; " +
                "-fx-text-fill: white;");

        Label label2 = new Label("Dog breeder and dog breeding\nservices in India");
        label2.setStyle("-fx-font-size: 20px; " +
                "-fx-font-family: 'Arial'; " +
                "-fx-text-fill: white;");

        Button readMoreButton = new Button("Read More...");
        readMoreButton.setStyle("-fx-font-size: 18px; " +
                "-fx-font-family: 'Elephant'; " +
                "-fx-background-color: transparent; " +
                "-fx-border-color: white; " +
                "-fx-text-fill: white;" +
                "-fx-arc-width: 25" );
        readMoreButton.setPadding(new Insets(10, 10, 10, 10));
        readMoreButton.setLayoutY(0);

        VBox demo = new VBox(30, label1, readMoreButton);
        demo.setAlignment(Pos.CENTER_LEFT);
        demo.setTranslateY(-5);
        demo.setPadding(new Insets(0, 0, 0, 50));

        // Create a VBox to hold label1, label2, and readMoreButton
        VBox labelBox = new VBox(label2);
        labelBox.setAlignment(Pos.CENTER_LEFT);
        labelBox.setTranslateY(-255);
        labelBox.setPadding(new Insets(0, 0, 0, 50)); // Adjust padding as needed

        // Create a HBox to hold the top left corner content
        HBox topLeftButtons = new HBox(10, homeButton, availablePuppiesButton, adoptionButton, loginButton, aboutUsButton);
        topLeftButtons.setPadding(new Insets(120, 0, 0, 0));
        topLeftButtons.setAlignment(Pos.TOP_RIGHT);

        Label label3 = new Label("Dog Sale In Pune");
        label3.setStyle("-fx-font-size: 100px; " +
                "-fx-font-family: 'Elephant'; " +
                "-fx-text-fill: black;");
        label3.setTranslateX(-50);
        label3.setTranslateY(-100);

        HBox labal3Box = new HBox(label3);
        labal3Box.setAlignment(Pos.BOTTOM_RIGHT);

        // Create a StackPane to hold the first image, doggoDiaryButton, topLeftContent, and additional image
        StackPane stackPane1 = new StackPane();
        stackPane1.getChildren().addAll(imageView1, doggoDiaryButton, topLeftButtons, socialLabel, additionalImageView, labelBox, demo, labal3Box);
        StackPane.setAlignment(doggoDiaryButton, Pos.TOP_CENTER); // Position doggoDiaryButton at the top center
        StackPane.setAlignment(topLeftButtons, Pos.TOP_LEFT); // Position topLeftContent at the top left
        StackPane.setAlignment(socialLabel, Pos.TOP_RIGHT); // Position socialLabel at the top right
        StackPane.setAlignment(additionalImageView, Pos.CENTER_RIGHT); // Position additionalImageView at center-right
        StackPane.setAlignment(labelBox, Pos.CENTER_LEFT); // Position labelBox at center-left
        StackPane.setAlignment(labal3Box, Pos.BOTTOM_RIGHT);

        // Create a new label for image2
        Label image2Label = new Label("Woof! Woof! That's the sound of angels in the guise of a furry dog, who have the\nability to fill up your life with pure, unadulterated joy. It is no wonder that 34% of\nIndians have a dog as a pet. Being one of the best Dog Breeders In Pune, we have\nbeen passionately connecting dog lovers with their favorite breeds and making their\nlife PAWSOME!\n\n" +
                "doggodiary : Get A Pooch To Bring Light & Happiness Into Your Life\n\n" +
                "Dogs are like your personal happiness squad, little balls of fur and joy. A pet dog will\nalways be there to shower you with loyalty with a lot of cuddles. You will always have\na source of unconditional emotional support when a dog with a wiggly tail is beside\nyou. Plus, they're like fitness expounders in disguise. Playing with them, taking them on\nwalks, or simply chasing them as they have taken your favorite t-shirt with some\nmischievous intention will make you embrace an active lifestyle. Oh, and did we\nmention the security? Dogs have got you covered! Their protective instincts and\nunwavering devotion will always keep you protected from potential threats!\n\n" +
                "In fact, they're adaptable too. These good boys and girls can be flexible; they adapt\nquickly to their surroundings and only seek love & attention. They are little social\nbutterflies who like to snoop around, and find a play partner. By choosing one for\nyourself from the Puppies For Sale In Pune, you will bring endless joy into your life.\nDogs teach responsibility and empathy. They're your playmate, stress-booster, and\nyour judgment-free comfort zone. But wait, there's more! They are tickets to outdoor\nadventures, connecting you with other pet lovers and paving the way for making new\nfriendships.\n\n" +
                "So, brace yourself for a life full of kisses, wagging tails, and a love that knows no\nboundaries. Contact us today for Dog For Sale In Pune!");
        image2Label.setStyle("-fx-font-size: 25px; " +
                "-fx-font-family: 'Cambria'; " +
                "-fx-text-fill: black;");
        image2Label.setWrapText(true);

        Label image2Label2 = new Label("Where Every Tail Wags with Joy...!");
        image2Label2.setStyle("-fx-font-size: 35px; " +
                "-fx-font-family: 'Cambria'; " +
                "-fx-text-fill: black;");
        image2Label2.setWrapText(true);
        image2Label2.setTranslateX(-420);
        image2Label2.setTranslateY(-20);

        VBox image2LabelBox = new VBox(image2Label2, image2Label);
        image2LabelBox.setAlignment(Pos.CENTER_RIGHT);
        image2LabelBox.setPadding(new Insets(0, 50, 50, 50));

        StackPane stackPane2 = new StackPane();
        stackPane2.getChildren().addAll(imageView2, image2LabelBox);
        StackPane.setAlignment(image2LabelBox, Pos.CENTER_RIGHT);

        // Create an HBox for the social media images
        HBox socialMediaBox = new HBox(10, socialImageView1, socialImageView2, socialImageView3);
        socialMediaBox.setAlignment(Pos.TOP_RIGHT);
        socialMediaBox.setPadding(new Insets(20, 20, 0, 0));
        stackPane1.getChildren().add(socialMediaBox);
        StackPane.setAlignment(socialMediaBox, Pos.TOP_RIGHT);

        // Add click events to the social media images
        socialImageView1.setOnMouseClicked(event -> openWebpage("https://www.instagram.com/prem_rajput.2225?igsh=dnU4aWQ1YWhza3Ex"));
        socialImageView2.setOnMouseClicked(event -> openWebpage("https://www.instagram.com/prem_rajput.2225?igsh=dnU4aWQ1YWhza3Ex"));
        socialImageView3.setOnMouseClicked(event -> openWebpage("https://www.instagram.com/prem_rajput.2225?igsh=dnU4aWQ1YWhza3Ex"));

        // Create labels for the counting effects
        Label counterLabel1 = new Label("+0");
        counterLabel1.setStyle("-fx-font-size: 80px; " +
                "-fx-font-family: 'Comic Sans MS'; " +
                "-fx-text-fill: white;");
        counterLabel1.setTranslateY(-150);
        counterLabel1.setTranslateX(-370);

        Label counterLabel2 = new Label("+0");
        counterLabel2.setStyle("-fx-font-size: 80px; " +
                "-fx-font-family: 'Comic Sans MS'; " +
                "-fx-text-fill: white;");
        counterLabel2.setTranslateY(-150);
        counterLabel2.setTranslateX(-20);

        Label counterLabel3 = new Label("+0");
        counterLabel3.setStyle("-fx-font-size: 80px; " +
                "-fx-font-family: 'Comic Sans MS'; " +
                "-fx-text-fill: white;");
        counterLabel3.setTranslateY(-150);
        counterLabel3.setTranslateX(370);

        // Create timelines for the counting effects
        Timeline timeline1 = createCountingTimeline(counterLabel1, 251);
        Timeline timeline2 = createCountingTimeline(counterLabel2, 165);
        Timeline timeline3 = createCountingTimeline(counterLabel3, 256);

        VBox VBox1 = new VBox(counterLabel1);
        VBox1.setAlignment(Pos.CENTER);

        VBox VBox2 = new VBox(counterLabel2);
        VBox2.setAlignment(Pos.CENTER);

        VBox VBox3 = new VBox(counterLabel3);
        VBox3.setAlignment(Pos.CENTER);

        HBox counterHBox = new HBox( VBox1, VBox2, VBox3 );
        counterHBox.setAlignment(Pos.CENTER);
        counterHBox.setSpacing(70);

        Label petspa = new Label("Pet Spa");
        petspa.setStyle("-fx-font-size: 60px; " +
                "-fx-font-family: 'Elephant'; " +
                "-fx-text-fill: white;");
        petspa.setTranslateX(230);
        petspa.setTranslateY(-180);

        Label vetanary = new Label("Vetanary");
        vetanary.setStyle("-fx-font-size: 60px; " +
                "-fx-font-family: 'Elephant'; " +
                "-fx-text-fill: white;");
        vetanary.setTranslateX(0);
        vetanary.setTranslateY(0);

        Label acceseries = new Label("Pet Spa");
        acceseries.setStyle("-fx-font-size: 60px; " +
                "-fx-font-family: 'Elephant'; " +
                "-fx-text-fill: white;");
        acceseries.setTranslateX(-230);
        acceseries.setTranslateY(-180);

        VBox petVBox = new VBox(petspa);
        petVBox.setAlignment(Pos.CENTER_LEFT);

        VBox vetanaryVBox = new VBox(vetanary);
        vetanaryVBox.setAlignment(Pos.CENTER);

        VBox acceseriesVBox = new VBox(acceseries);
        acceseriesVBox.setAlignment(Pos.CENTER_RIGHT);

        HBox page3LabelHBox = new HBox(20, petVBox, vetanaryVBox, acceseriesVBox);
        page3LabelHBox.setAlignment(Pos.CENTER_LEFT);

        Button doggoDiaryButton1= new Button("doggodiary");
        doggoDiaryButton1.setPadding(new Insets(0, 0, 100, 60));
        doggoDiaryButton1.setStyle("-fx-font-size: 60px; " +
                "-fx-font-family: 'Elephant'; " +
                "-fx-background-color: transparent; " +
                "-fx-border-color: transparent; " +
                "-fx-text-fill: #f2a30a;");
        
        Label makeYourEnquiry = new Label("Make Your Enquiry");
        makeYourEnquiry.setStyle("-fx-font-size: 40px; " +
                "-fx-font-family: 'Elephant'; " +
                "-fx-text-fill: black;");
        //makeYourEnquiry.setTranslateX(0);
        //makeYourEnquiry.setTranslateY(-100);
        makeYourEnquiry.setPadding(new Insets(0,0,50,60));

        Label phone = new Label("+91 79724 26146");
        phone.setStyle("-fx-font-size: 20px; " +
                "-fx-font-family: 'Elephant'; " +
                "-fx-text-fill: black;");
        phone.setPadding(new Insets(0,0,100,60));

        Label address = new Label("3rd Floor, Walhekar Properties, Core2web\nTechnologies, Narhe, above HDFC Bank, Narhe\nPune, Maharashtra 411041");
        address.setStyle("-fx-font-size: 20px; " +
                "-fx-font-family: 'Elephant'; " +
                "-fx-text-fill: black;");
        address.setPadding(new Insets(0,0,100,60));
        
        Label email = new Label("www.doggodiary.com");
        email.setStyle("-fx-font-size: 20px; " +
                "-fx-font-family: 'Elephant'; " +
                "-fx-text-fill: black;");
        phone.setPadding(new Insets(0,0,120,80));
        
        
        VBox buttonVBox = new VBox(doggoDiaryButton1, makeYourEnquiry, phone, email, address);
        buttonVBox.setAlignment(Pos.BOTTOM_LEFT);
        

        // Add the counter labels to stackPane3
        StackPane stackPane3 = new StackPane();
        stackPane3.getChildren().addAll(imageView3, counterHBox, page3LabelHBox, buttonVBox);
        StackPane.setAlignment(counterHBox, Pos.CENTER);

        // Create a VBox to hold the stackPane and other images
        VBox vbox = new VBox(stackPane1, stackPane2, stackPane3);
        vbox.setAlignment(Pos.CENTER);
         for (int i = 0; i < 15; i++) {
            vbox.getChildren().add(new Text(""));

        }

        // Create a ScrollPane to make the VBox scrollable
        ScrollPane scrollPane = new ScrollPane(vbox);
        scrollPane.setFitToWidth(true);

        // Add a listener to the ScrollPane's viewport to detect when the user reaches page 3
         scrollPane.vvalueProperty().addListener((obs, oldVal, newVal) -> {
            // Calculate the position to start the counters (for page 3)
            double pageHeight = vbox.getHeight() / 3;
            double scrollPosition = scrollPane.getVvalue() * vbox.getHeight();

            if (scrollPosition >= 2 * pageHeight) {
                if (!timeline1.getStatus().equals(Timeline.Status.RUNNING)) {
                    timeline1.play();
                }
                if (!timeline2.getStatus().equals(Timeline.Status.RUNNING)) {
                    timeline2.play();
                }
                if (!timeline3.getStatus().equals(Timeline.Status.RUNNING)) {
                    timeline3.play();
                }
            }
        });
        // Create the scene and set it on the primary stage
        Scene scene = new Scene(scrollPane, 1980, 1080);
        primaryStage.setTitle("Image Scroller");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
    // Method to open a webpage in the default browser
    private void openWebpage(String url) {
        try {
            Desktop.getDesktop().browse(new URI(url));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Method to create a counting timeline for a label
    private Timeline createCountingTimeline(Label label, int maxCount) {
        Timeline timeline = new Timeline();
        KeyFrame keyFrame = new KeyFrame(Duration.millis(20), event -> {
            String text = label.getText().substring(1);
            int count = Integer.parseInt(text);
            if (count < maxCount) {
                count++;
                label.setText("+" + count);
            }
        });
        timeline.getKeyFrames().add(keyFrame);
        timeline.setCycleCount(maxCount);
        return timeline;
    }
}
