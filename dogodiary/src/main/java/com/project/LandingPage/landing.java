package com.project.LandingPage;

import java.awt.Desktop;
import java.net.URI;

import com.project.controller.LoginPage;
import com.project.shop.accesories;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class landing extends Application {
  

    @Override
    public void start(Stage primaryStage) throws Exception {


        Image socialImage1 = new Image("file:dogodiary\\src\\main\\resources\\images\\facbook.png");
        Image socialImage2 = new Image("file:dogodiary\\src\\main\\resources\\images\\insta logo.png");
        Image socialImage3 = new Image("file:dogodiary\\src\\main\\resources\\images\\file (4).png");

        ImageView socialImageView1 = new ImageView(socialImage1);
        ImageView socialImageView2 = new ImageView(socialImage2);
        ImageView socialImageView3 = new ImageView(socialImage3);
        socialImageView1.setOnMouseClicked(event -> openWebpage("https://www.instagram.com/prem_rajput.2225?igsh=dnU4aWQ1YWhza3Ex"));
        socialImageView2.setOnMouseClicked(event -> openWebpage("https://www.instagram.com/prem_rajput.2225?igsh=dnU4aWQ1YWhza3Ex"));
        socialImageView3.setOnMouseClicked(event -> openWebpage("https://www.instagram.com/prem_rajput.2225?igsh=dnU4aWQ1YWhza3Ex"));

        socialImageView1.setFitWidth(20);
        socialImageView1.setPreserveRatio(true);

        socialImageView2.setFitWidth(20);
        socialImageView2.setPreserveRatio(true);

        socialImageView3.setFitWidth(25);
        socialImageView3.setPreserveRatio(true);

        HBox socialMediaBox = new HBox(10, socialImageView1, socialImageView2, socialImageView3);
        socialMediaBox.setAlignment(Pos.TOP_RIGHT);
        socialMediaBox.setPadding(new Insets(10, 10, 0, 0));
        socialMediaBox.setBackground(new Background(new BackgroundFill(Color.DARKTURQUOISE, CornerRadii.EMPTY, Insets.EMPTY)));

        //StackPane.setAlignment(socialMediaBox, Pos.TOP_RIGHT);

        
    
        Image dogoImage = new Image("images/logo.jpg");
        ImageView dogoImageView = new ImageView(dogoImage);
        dogoImageView.setFitHeight(60);
        dogoImageView.setFitWidth(70);
        Button home = new Button("doggodiary");
        home.setTextFill(Color.ORANGERED);
        home.setAlignment(Pos.BASELINE_LEFT);
        home.setFont(new Font("elephant",25));
        home.setMaxHeight(80);
        home.setPrefWidth(350);
        home.setStyle("-fx-background-color:white");
        home.setStyle("-fx-border-color:black");
        home.setEffect(new DropShadow(20, Color.WHITE));
       home.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
       home.setGraphic(dogoImageView);
        
        Button homeService = new Button("HOME");
        homeService.setTextFill(Color.GREENYELLOW);
        homeService.setAlignment(Pos.CENTER);
        homeService.setFont(new Font("Serif Regular",20));
        homeService.setPrefHeight(80);
        homeService.setPrefWidth(190);
        homeService.setStyle("-fx-background-color:white");
        homeService.setStyle("-fx-border-color:black");
      //  homeService.setEffect(new DropShadow(20, Color.BLACK));
       homeService.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

       Button availPuppy = new Button("AVAILABLE \nPUPPIES");
       availPuppy.setTextFill(Color.GREENYELLOW);
       availPuppy.setAlignment(Pos.CENTER);
       availPuppy.setFont(new Font("Serif Regular",20));
       availPuppy.setPrefHeight(80);
       availPuppy.setPrefWidth(150);
       availPuppy.setStyle("-fx-background-color:white");
       availPuppy.setStyle("-fx-border-color:black");
      // availPuppy.setEffect(new DropShadow(20, Color.BLACK));
      availPuppy.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

      Button adopttion = new Button("ADOPTION");
      adopttion.setTextFill(Color.GREENYELLOW);
      adopttion.setAlignment(Pos.CENTER);
      adopttion.setFont(new Font("Serif Regular",20));
      adopttion.setPrefHeight(80);
      adopttion.setPrefWidth(150);
      adopttion.setStyle("-fx-background-color:white");
      adopttion.setStyle("-fx-border-color:black");
    //  adopttion.setEffect(new DropShadow(20, Color.BLACK));
      adopttion.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

       
        
        Image shopImage = new Image("images/shop.jpg");
        ImageView cartView = new ImageView(shopImage);
        cartView.setFitHeight(60);
        cartView.setFitWidth(70);
        Button shop = new Button("SHOP");
        shop.setTextFill(Color.GREENYELLOW);
        shop.setAlignment(Pos.CENTER);
        shop.setFont(new Font("Serif Regular",20));  
        shop.setPrefHeight(80);
        shop.setPrefWidth(190); 
        shop.setStyle("-fx-border-color:black");
       // shop.setStyle("-fx-background-color:white");
       // shop.setEffect(new DropShadow(20, Color.BLACK));
        shop.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
        shop.setGraphic(cartView);
        shop.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                Scene scene = new accesories().CreateScene(primaryStage);
                primaryStage.setScene(scene);
                
                }
            });
           
        Button login = new Button("LOGIN");
        login.setTextFill(Color.GREENYELLOW);
        login.setAlignment(Pos.CENTER);
        login.setFont(new Font("serif Regular",20));
        login.setPrefHeight(80);
        login.setPrefWidth(150);
       // login.setStyle("-fx-background-color:yellow");
        login.setStyle("-fx-border-color:black");
       // login.setEffect(new DropShadow(20, Color.BLACK));
       login.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

       login.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            
            Scene scene = new LoginPage().initializeLoginScene(primaryStage);
            primaryStage.setScene(scene);
            
            }
        });
        Button about = new Button("ABOUT");
        about.setTextFill(Color.GREENYELLOW);
        about.setAlignment(Pos.BASELINE_LEFT);
        about.setFont(new Font("serif regular",20));
        about.setMaxHeight(80);
        about.setPrefWidth(150);
        about.setStyle("-fx-background-color:white");
        about.setStyle("-fx-border-color:black");
       // about.setEffect(new DropShadow(20, Color.BLACK));
        about.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
        HBox hb1 = new HBox(home,homeService,availPuppy,adopttion,shop,login,about);
        hb1.setPrefHeight(80);
        hb1.setPrefWidth(1980);
        

        // Create ImageViews to display the images
        Image image = new Image("file:dogodiary\\src\\main\\resources\\images\\peak.jpg");
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(400);
        imageView.setFitWidth(750);
       // imageView.setPreserveRatio(true);
        // Create the text
        Label label = new Label("'Happiness comes\n" + //
                        "in four legs, has a\n" + //
                        "furry body and\n" + //
                        "joyful wagging tail'");
        label.setFont(Font.font("Arial", FontWeight.BOLD, 50));
        label.setTextFill(Color.WHITE);
        // Create a StackPane to overlay the text on the image
        Button readMoreButton = new Button("Read More...");
        readMoreButton.setStyle("-fx-font-size: 18px; " +
                "-fx-font-family: 'Elephant'; " +
                "-fx-background-color: transparent; " +
                "-fx-border-color: white; " +
                "-fx-text-fill: white;" +
                "-fx-arc-width: 25" );
        readMoreButton.setPadding(new Insets(10, 10, 10, 10));
        readMoreButton.setLayoutY(0);
        StackPane stackPane = new StackPane();
        stackPane.getChildren().addAll(imageView, label,readMoreButton);
        StackPane.setAlignment(label, javafx.geometry.Pos.CENTER_LEFT);
        StackPane.setAlignment(readMoreButton, javafx.geometry.Pos.BOTTOM_LEFT);


        Image mImage2 = new Image("file:dogodiary\\src\\main\\resources\\images\\peakpx (4).jpg");
        ImageView mImageView2 = new ImageView(mImage2);
        mImageView2.setFitHeight(400);
        mImageView2.setFitWidth(530);
        HBox middleBox = new HBox(stackPane,mImageView2);
        middleBox.setPrefHeight(400);
        middleBox.setPrefWidth(1100);

        Circle circle1 = new Circle();
        circle1.setFill(new ImagePattern(image));
        circle1.setCenterX(100.0f);
        circle1.setCenterY(100.0f);
        circle1.setRadius(70.0f);
      //  circle1.setFill(Color.BLACK);
        Circle circle2 = new Circle();
        circle2.setCenterX(100.0f);
        circle2.setCenterY(100.0f);
        circle2.setRadius(70.0f);
        circle2.setFill(Color.BLACK);
        Circle circle3 = new Circle();
        circle3.setCenterX(100.0f);
        circle3.setCenterY(100.0f);
        circle3.setRadius(70.0f);
        circle3.setFill(Color.BLACK);
        Label label3 = new Label("Dog Sale In Pune");
        label3.setStyle("-fx-font-size: 50px; " +
                "-fx-font-family: 'Elephant'; " +
                "-fx-text-fill: black;");
        HBox labal3Box = new HBox(label3);
        labal3Box.setAlignment(Pos.CENTER_RIGHT);
        HBox circleBox = new HBox(90,circle1,circle2,circle3,labal3Box);
        circleBox.setTranslateY(10);
        circleBox.setTranslateX(20);
       // circleBox.setStyle("-fx-border-color:black");
        //circleBox.setEffect(new DropShadow(20, Color.BLACK));
        
        Image page2Image = new Image("file:dogodiary\\src\\main\\resources\\images\\page2.png");
        ImageView page2ImageView = new ImageView(page2Image);
        page2ImageView.setFitHeight(700);
        page2ImageView.setFitWidth(1100);
       // imageView.setPreserveRatio(true);
        // Create the text

                // Create a new label for image2
        Label image2Label = new Label("Woof! Woof! That's the sound of angels in the guise of a furry dog, who have the\nability to fill up your life with pure, unadulterated joy. It is no wonder that 34% of\nIndians have a dog as a pet. Being one of the best Dog Breeders In Pune, we have\nbeen passionately connecting dog lovers with their favorite breeds and making their\nlife PAWSOME!\n\n" +
                "doggodiary : Get A Pooch To Bring Light & Happiness Into Your Life\n\n" +
                "Dogs are like your personal happiness squad, little balls of fur and joy. A pet dog will\nalways be there to shower you with loyalty with a lot of cuddles. You will always have\na source of unconditional emotional support when a dog with a wiggly tail is beside\nyou. Plus, they're like fitness expounders in disguise. Playing with them, taking them on\nwalks, or simply chasing them as they have taken your favorite t-shirt with some\nmischievous intention will make you embrace an active lifestyle. Oh, and did we\nmention the security? Dogs have got you covered! Their protective instincts and\nunwavering devotion will always keep you protected from potential threats!\n\n" +
                "In fact, they're adaptable too. These good boys and girls can be flexible; they adapt\nquickly to their surroundings and only seek love & attention. They are little social\nbutterflies who like to snoop around, and find a play partner. By choosing one for\nyourself from the Puppies For Sale In Pune, you will bring endless joy into your life.\nDogs teach responsibility and empathy. They're your playmate, stress-booster, and\nyour judgment-free comfort zone. But wait, there's more! They are tickets to outdoor\nadventures, connecting you with other pet lovers and paving the way for making new\nfriendships.\n\n" +
                "So, brace yourself for a life full of kisses, wagging tails, and a love that knows no\nboundaries. Contact us today for Dog For Sale In Pune!");
        image2Label.setStyle("-fx-font-size: 15px; " +
                "-fx-font-family: 'Cambria'; " +
                "-fx-text-fill: black;");
        image2Label.setWrapText(true);

        Label image2Label2 = new Label("Where Every Tail Wags with Joy...!");
        image2Label2.setStyle("-fx-font-size: 20px; " +
                "-fx-font-family: 'Cambria'; " +
                "-fx-text-fill: black;");
        image2Label2.setWrapText(true);
        image2Label2.setTranslateX(-320);
        image2Label2.setTranslateY(-20);

        VBox image2LabelBox = new VBox(image2Label2, image2Label);
        image2LabelBox.setAlignment(Pos.CENTER_RIGHT);
        image2LabelBox.setPadding(new Insets(0, 50, 50, 50));

        StackPane stackPane3 = new StackPane();
        stackPane3.getChildren().addAll(page2ImageView, image2LabelBox);
        StackPane.setAlignment(image2LabelBox, Pos.CENTER_RIGHT);
        stackPane3.setTranslateY(20);

        Button doggoDiaryButton1= new Button("doggodiary");
        doggoDiaryButton1.setTranslateY(300);
        doggoDiaryButton1.setPadding(new Insets(0, 0, 40, 60));
        doggoDiaryButton1.setStyle("-fx-font-size: 60px; " +
                "-fx-font-family: 'Elephant'; " +
                "-fx-background-color: transparent; " +
                "-fx-border-color: transparent; " +
                "-fx-text-fill: #f2a30a;");
        
        Label makeYourEnquiry = new Label("Make Your Enquiry");
        makeYourEnquiry.setTranslateY(270);
        makeYourEnquiry.setStyle("-fx-font-size: 40px; " +
                "-fx-font-family: 'Elephant'; " +
                "-fx-text-fill: black;");
        //makeYourEnquiry.setTranslateX(0);
        //makeYourEnquiry.setTranslateY(-100);
        makeYourEnquiry.setPadding(new Insets(0,0,50,60));

        Label phone = new Label("+91 79724 26146");
        phone.setTranslateY(380);
        phone.setTranslateX(250);
        phone.setStyle("-fx-font-size: 20px; " +
                "-fx-font-family: 'Elephant'; " +
                "-fx-text-fill: black;");
        phone.setPadding(new Insets(0,0,100,60));

        Label address = new Label("3rd Floor, Walhekar Properties, Core2web\nTechnologies, Narhe, above HDFC Bank, Narhe\nPune, Maharashtra 411041");
        address.setTranslateY(150);
        address.setStyle("-fx-font-size: 20px; " +
                "-fx-font-family: 'Elephant'; " +
                "-fx-text-fill: black;");
        address.setPadding(new Insets(0,0,100,60));
        
        Label email = new Label("www.doggodiary.com");
        email.setTranslateY(140);
        email.setStyle("-fx-font-size: 20px; " +
                "-fx-font-family: 'Elephant'; " +
                "-fx-text-fill: black;");
        phone.setPadding(new Insets(0,0,120,80));
        
        
        VBox buttonVBox = new VBox(doggoDiaryButton1, makeYourEnquiry, phone, email, address);
        buttonVBox.setAlignment(Pos.BOTTOM_LEFT);
        
        HBox lastPage = new HBox(buttonVBox);
        BackgroundImage myBI= new BackgroundImage(new Image("images/page3.png",1280,750,false,true),
        BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
        BackgroundSize.DEFAULT);       //then you set to your node*/
        lastPage.setBackground(new Background(myBI));



        VBox vBox = new VBox(socialMediaBox,hb1,middleBox,circleBox,stackPane3,lastPage);
        
        for(int i =0;i<4;i++){
            vBox.getChildren().add(new Text(""));

        }
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(vBox);
       // scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);

        Scene scene = new Scene(scrollPane,primaryStage.getWidth(),primaryStage.getHeight());
       
        
        primaryStage.setScene(scene);
        primaryStage.show();
    }
     private void openWebpage(String url) {
        try {
            Desktop.getDesktop().browse(new URI(url));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}